from django.shortcuts import render
from django.http import HttpResponse
from .models import *
from datetime import datetime
import time


def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")


def pedidoLoQueSea(request):
    
    if request.method =="POST":
        if request.POST['nextStep'] == "2":
            ciudades = Ciudad.objects.all()
            pedidoLoQueSea = request.POST["pedidoLoQueSea"]

            pedido = PedidoLoQueSea()
            pedido.textoPedido = pedidoLoQueSea
            if request.FILES.get("img", ""):
                pedido.fotoPedido = request.FILES["img"]
            
            todosLosPedidos = PedidoLoQueSea.objects.all()
            if len(todosLosPedidos) > 0:
                pedidoMayorId = todosLosPedidos.order_by("-id")[0]
                mayorId = pedidoMayorId.id
                pedido.id = mayorId + 1
            else:
                pedido.id = 1

            idPedido = pedido.id
            pedido.save()

            return render(request,
                        'pedidoLoQueSea2.html',
                         {"ciudades": ciudades, "idPedido": idPedido})

        elif request.POST['nextStep'] == "3":
            ciudades = Ciudad.objects.all()

            idPedido = request.POST["idPedido"]
            pedido = PedidoLoQueSea.objects.get(id = idPedido)

            if request.POST.get("coordenadas", "") == "":
                calleComercio = request.POST["calleComercio"]
                numeroComercio = int( request.POST["numeroComercio"] )
                idCiudad = int(request.POST["ciudadComercio"])
                ciudadComercio = Ciudad.objects.get( id =  idCiudad)
                referenciaComercio = request.POST["referenciaComercio"]

                pedido.calleComercio = calleComercio
                pedido.numeroComercio = numeroComercio
                pedido.ciudadComercio = ciudadComercio
                if referenciaComercio != "":
                    pedido.referenciaComercio = referenciaComercio
                
                pedido.save()

                print("----------------------EXITO ingresando el comercio")
            else:
                coordenadasComercio = request.POST["coordenadas"].split(",")
                latitudComercio = float(coordenadasComercio[0])
                longitudComercio =float(coordenadasComercio[1])

                pedido.latitudComercio = latitudComercio
                pedido.longitudComercio = longitudComercio

                pedido.save()

                print("---------LATITUD COMERCIO: " + str(latitudComercio))
                print("---------LONGITUD COMERCIO: " + str(longitudComercio))

            return render(request,
                         'pedidoLoQueSea3.html',
                          {"ciudades": ciudades, "idPedido": idPedido})

        elif request.POST['nextStep'] == "4":

            idPedido = int( request.POST["idPedido"] )
            pedido = PedidoLoQueSea.objects.get(id = idPedido)

            calleEntrega = request.POST["calleEntrega"]
            numeroEntrega = int( request.POST["numeroEntrega"] )
            idCiudadEntrega = int(request.POST["ciudadEntrega"])
            ciudadEntrega = Ciudad.objects.get(id = idCiudadEntrega)
            referenciaEntrega = request.POST["referenciaEntrega"]

            pedido.calleEntrega = calleEntrega
            pedido.numeroEntrega = numeroEntrega
            pedido.ciudadEntrega = ciudadEntrega
            if referenciaEntrega != "":
                pedido.referenciaEntrega = referenciaEntrega

            pedido.save()

            return render(request,
                         'pedidoLoQueSea4.html',
                          {"idPedido": idPedido})

        elif request.POST['nextStep'] == "5":

            idPedido = int( request.POST["idPedido"] )
            pedido = PedidoLoQueSea.objects.get(id = idPedido)

            if request.POST['medioPago'] == '1':
                return render(request,
                             'pedidoLoQueSea5-1.html',
                              {"idPedido": idPedido})
            else:
                mesMinimo = datetime.now().month
                anoMinimo = datetime.now().year - 2000
                return render(request,
                             'pedidoLoQueSea5-2.html',
                              {"idPedido": idPedido,
                               "mesMinimo": mesMinimo,
                               "anoMinimo": anoMinimo})
        
        elif request.POST['nextStep'] == "6":

            idPedido = int( request.POST["idPedido"] )
            pedido = PedidoLoQueSea.objects.get(id = idPedido)

            if "montoEfectivo" in request.POST:
                montoEfectivo = int( request.POST["montoEfectivo"] )
                pedido.montoEfectivo = montoEfectivo
            else:
                nombreTarjeta = request.POST["nombreTarjeta"]
                numeroTarjeta = int( request.POST["numTarjeta"] )
                cvvTarjeta = int( request.POST["cvvTarjeta"] )
                mesTarjeta = int( request.POST["mesExpTarjeta"] )
                anoTarjeta = int( request.POST["anoExpTarjeta"] )

                pedido.nombreTarjeta = nombreTarjeta
                pedido.numeroTarjeta = numeroTarjeta
                pedido.cvvTarjeta = cvvTarjeta
                pedido.mesTarjeta = mesTarjeta
                pedido.anoTarjeta = anoTarjeta

            pedido.save()

            return render(request,
                         'pedidoLoQueSea6.html',
                          {"idPedido": idPedido})

        elif request.POST['nextStep'] == "7":

            idPedido = int( request.POST["idPedido"] )
            pedido = PedidoLoQueSea.objects.get(id = idPedido)

            if request.POST["recepcionPedido"] != "1":
                try:
                    anno = int(request.POST["fechaInput"][0:4])
                    mes = int(request.POST["fechaInput"][5:7])
                    dia = int(request.POST["fechaInput"][8:])

                    hora = int( request.POST["horaInput"][0:2] )

                    datetimeRecepcion = datetime(anno, mes, dia, hora)
                except:
                    datetimeRecepcion = datetime.now()

                pedido.datetimeRecepcion = datetimeRecepcion

                pedido.save()

            return render(request, 'pedidoLoQueSea7.html')

    else:
        return render(request, 'pedidoLoQueSea.html')