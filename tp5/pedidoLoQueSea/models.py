from django.core.files.storage import FileSystemStorage
from django.db import models

fs = FileSystemStorage(location = './fotosPedidos')

class PedidoLoQueSea(models.Model):
    id=models.IntegerField(primary_key=True, auto_created=True, editable=False)
    textoPedido = models.CharField(max_length=100, null=True, blank=True)
    fotoPedido = models.ImageField(upload_to=fs, null=True, blank=True)

    calleComercio = models.CharField(max_length=100, null=True, blank=True)
    numeroComercio = models.IntegerField(null=True, blank=True)
    ciudadComercio = models.ForeignKey('Ciudad', on_delete=models.SET_NULL,
                                         related_name="ciudadComercio",
                                          null=True, blank = True)
    referenciaComercio = models.CharField(max_length=200,
                                         null=True, blank=True)
    latitudComercio = models.FloatField(null=True, blank=True)
    longitudComercio = models.FloatField(null=True, blank=True)

    calleEntrega = models.CharField(max_length=100, null=True, blank=True)
    numeroEntrega = models.IntegerField(null=True, blank=True)
    ciudadEntrega = models.ForeignKey('Ciudad', on_delete=models.SET_NULL,
                                     related_name="ciudadEntrega",
                                      null=True, blank = True)
    referenciaEntrega = models.CharField(max_length=200, null=True, blank=True)

    montoEfectivo = models.IntegerField(null=True, blank=True)

    nombreTarjeta = models.CharField(max_length=100, null=True, blank=True)
    numeroTarjeta = models.IntegerField(null=True, blank=True)
    cvvTarjeta = models.IntegerField(null=True, blank=True)
    mesTarjeta = models.IntegerField(null=True, blank=True)
    anoTarjeta = models.IntegerField(null=True, blank=True)

    datetimeRecepcion = models.DateTimeField(null=True, blank=True)



    def __str__(self):
        return "Pedido #" + str(self.id)

    
    

class Ciudad(models.Model):
    id=models.IntegerField(primary_key=True, auto_created=True, editable=False)
    nombre = models.CharField(max_length=100)

    def __str__(self):
        return self.nombre