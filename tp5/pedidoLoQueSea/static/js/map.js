console.log("pito")
function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: -31.4271928, lng: -64.1766328},
        zoom: 14
    });
    var marker = new google.maps.Marker({position: {lat: -31.4271928, lng: -64.1766328} , map: map});
    var new_value
    map.addListener('click', function(e) {
        marker.setPosition(e.latLng);
        new_value=`${marker.position.lat()}, ${marker.position.lng()}`;
        document.getElementById("map-input").setAttribute('value', new_value);
    });
}