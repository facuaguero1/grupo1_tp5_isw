# Generated by Django 3.0.6 on 2020-05-07 17:09

import django.core.files.storage
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pedidoLoQueSea', '0003_auto_20200507_1703'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pedidoloquesea',
            name='fotoPedido',
            field=models.ImageField(blank=True, null=True, upload_to=django.core.files.storage.FileSystemStorage(location='./fotosPedidos')),
        ),
    ]
